package ex02;

public class StandardUser extends LimitedUser implements IPlayVideo {
	@Override
	public void playMusic() {
		System.out.println("01 Music Played");
	}

	@Override
	public void playVideo() {
		System.out.println("02 Video played");
		
	}

}
