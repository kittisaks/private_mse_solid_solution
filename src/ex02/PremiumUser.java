package ex02;

public class PremiumUser extends StandardUser implements IPlayVideoHD{

	@Override
	public void playMusic() {
		System.out.println("01 Music Played");
	}

	@Override
	public void playVideo() {
		System.out.println("02 Video played");
		
	}

	@Override
	public void playVideoHD() {
		System.out.println("03 Video HD played");
	}

}
