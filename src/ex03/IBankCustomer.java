package ex03;

public interface IBankCustomer {
	public void setName(String name);
	public String getName();
	
	public void setTelephone(String telephone);
	public String geTelephone();
	
	public void setOrgName(String org);
	public String getOrgName();
	
}
