package ex04;

public class keyValueDb implements IDbWriter {

	public keyValueDb(String host, int port) {
		
	}
	
	public void writeCustList(CustomerList custList) {
		connect();
		write(custList);
		close();
	}
	
	public void connect() {
		System.out.println("keyValueDb - connect");
	}
	
	public void write(CustomerList custList) {
		System.out.println("keyValueDb - writing");
	}
	
	public void close() {
		System.out.println("keyValueDb - close");
	}
}
