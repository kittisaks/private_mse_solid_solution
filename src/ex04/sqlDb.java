package ex04;

public class sqlDb implements IDbWriter {
	
	public sqlDb(String host, int port) {
		
	}
	
	public void writeCustList(CustomerList custList) {
		connect();
		write(custList);
		close();
	}
	
	public void connect() {
		System.out.println("sqlDb - connect");
	}
	
	public void write(CustomerList custList) {
		System.out.println("sqlDb - write");
	}
	
	public void close() {
		System.out.println("sqlDb - close");
	}
}
