package ex04;

public class fileDb implements IDbWriter {

	public fileDb(String filename) {
		
	}
	
	public void writeCustList(CustomerList custList) {
		open();
		write(custList);
		close();
	}
	
	public void open() {
		System.out.println("fileDb - opening");
	}
	
	public void write(CustomerList custList) {
		System.out.println("fileDb - writing");
	}
	
	public void close() {
		System.out.println("fileDb - closing");
	}
}
