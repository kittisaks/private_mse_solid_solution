package ex04;

public class noSqlDb implements IDbWriter {
	public noSqlDb(String host, int port) {
		
	}
	
	public void writeCustList(CustomerList custList) {
		connect();
		write(custList);
		close();
	}
	
	public void connect() {
		System.out.println("noSqlDb - Connect");
	}
	
	public void write(CustomerList custList) {
		System.out.println("noSqlDb - Write");
	}
	
	public void close() {
		System.out.println("noSqlDb - Close");
	}
}
