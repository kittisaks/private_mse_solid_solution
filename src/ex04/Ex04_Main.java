package ex04;

public class Ex04_Main {
	public static void main(String[] args){
		
		IDbWriter w0 = new fileDb("test.txt");
		IDbWriter w1 = new keyValueDb("localhost", 9090);
		IDbWriter w2 = new noSqlDb("localhost", 9091);
		IDbWriter w3 = new sqlDb("localhost", 9092);
		
		
		CustomerList cl0 = new CustomerList(w0);
		cl0.ImportToDb();
		
		CustomerList cl1 = new CustomerList(w1);
		cl1.ImportToDb();
		
		CustomerList cl2 = new CustomerList(w2);
		cl2.ImportToDb();
		
		CustomerList cl3 = new CustomerList(w3);
		cl3.ImportToDb();
		
	}
}
