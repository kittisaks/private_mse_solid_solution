package ex04;

public class CustomerList {

	private IDbWriter dbWriter = null;
	
	public CustomerList(IDbWriter writer) {
		dbWriter = writer;
	}
	
	public void ImportToDb() {
		dbWriter.writeCustList(this);
	}
	
}
