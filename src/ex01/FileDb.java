package ex01;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileDb {
	
	private String fileDb = null;
	
	public FileDb(String fileDb) {
		this.fileDb = fileDb;
	}
	
	public boolean isFileDbExist(String fileDb) {
		File db = new File(fileDb);
		return db.exists();
	}
	
	public File createFileDb(String fileDb) {
		File db = new File(fileDb);
		try {
			if (db.createNewFile())
				return db;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void addUserToFileDb(String fileDb, String name, String address, String phone) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileDb, true));
			writer.newLine();
			writer.write(name + ", " + address + ", " + phone);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
