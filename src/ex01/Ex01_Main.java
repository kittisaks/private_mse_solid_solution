package ex01;

// Single Responsibility

public class Ex01_Main {
	public static void main(String[] args){

		System.out.println("This is example 01");
		User test = new User("Kitty", "Wow", "My Phone");
		test.Register();
	}
}
