package ex01;

public class User {
	
	private String name;
	private String address;
	private String phone;
	private final String fileDb = "userdb.txt";
	private FileDb dbService = new FileDb(fileDb);	
	
	public User(String name, String address, String phone) {
		this.name = name;
		this.address = address;
		this.phone = phone;
	}
	
	public void Register() {
		
		if (!dbService.isFileDbExist(fileDb))
			dbService.createFileDb(fileDb);
		
		dbService.addUserToFileDb(fileDb, name, address, phone);
	}
	
}
