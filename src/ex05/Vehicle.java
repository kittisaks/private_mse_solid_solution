package ex05;

public class Vehicle {
	
	VehicleType type = null;
	
	public Vehicle(VehicleType type) {
		this.type = type;
	}
	
	public void run() {
		System.out.println("RUN!!!");
	}
	
	public void load() throws Exception {
		if ((type != VehicleType.Truck) && (type != VehicleType.Plane))
			throw new Exception();
		System.out.println("LOAD!!!");
	}
	
	public void fly() throws Exception {
		if (type != VehicleType.Plane)
			throw new Exception();
		System.out.println("FLY!!!");
	}
}
