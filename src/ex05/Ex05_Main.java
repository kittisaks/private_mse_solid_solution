package ex05;

//Open-Close

public class Ex05_Main {
	public static void main(String[] args){
		
		Car car = new Car();
		Truck truck = new Truck();
		Plane plane = new Plane();
		
			plane.run();
			plane.load();
			plane.fly();
			
			truck.run();
			truck.load();
			//truck.fly();
			
			car.run();
			//car.load();
			//car.fly();
	}
}
